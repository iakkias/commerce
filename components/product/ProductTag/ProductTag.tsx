interface ProductTagProps {
  className?: string
  name: string
  price: string
  fontSize?: number
}

const ProductTag: React.FC<ProductTagProps> = ({ name, price }) => {
  return (
    <>
      <h3 className="mt-4 text-base font-semibold text-gray-900">{name}</h3>
      <p className="mt-1 text-sm text-gray-500">{price}</p>
    </>
  )
}

export default ProductTag
