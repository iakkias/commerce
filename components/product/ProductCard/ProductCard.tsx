import { FC } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import type { Product } from '@commerce/types/product'
import s from './ProductCard.module.css'
import Image, { ImageProps } from 'next/image'
import WishlistButton from '@components/wishlist/WishlistButton'
import usePrice from '@framework/product/use-price'
import ProductTag from '../ProductTag'

interface Props {
  className?: string
  product: Product
  noNameTag?: boolean
  imgProps?: Omit<ImageProps, 'src' | 'layout' | 'placeholder' | 'blurDataURL'>
  variant?: 'default' | 'slim' | 'simple'
}

const placeholderImg = '/product-img-placeholder.svg'

const ProductCard: FC<Props> = ({
  product,
  imgProps,
  className,
  noNameTag = false,
  variant = 'default',
}) => {
  const { price } = usePrice({
    amount: product.price.value,
    baseAmount: product.price.retailPrice,
    currencyCode: product.price.currencyCode!,
  })

  const rootClassName = cn(
    s.root,
    { [s.slim]: variant === 'slim', [s.simple]: variant === 'simple' },
    className
  )

  return (
    <div className="relative group">
      {variant === 'slim' && (
        <>
          <div className={s.header}>
            <span>{product.name}</span>
          </div>

          {product?.images && (
            <Image
              quality="85"
              src={product.images[0]?.url || placeholderImg}
              alt={product.name || 'Product Image'}
              height={320}
              width={320}
              layout="fixed"
              {...imgProps}
            />
          )}
        </>
      )}

      {variant === 'simple' && (
        <>
          {process.env.COMMERCE_WISHLIST_ENABLED && (
            <WishlistButton
              className={s.wishlistButton}
              productId={product.id}
              variant={product.variants[0]}
            />
          )}

          <div className="w-full overflow-hidden rounded-lg h-96 group-hover:opacity-75 sm:h-auto sm:aspect-w-2 sm:aspect-h-3">
            {product?.images && (
              <Image
                layout="fill"
                alt={product.name || 'Product Image'}
                src={product.images[0]?.url || placeholderImg}
                quality="85"
                className="object-cover object-center w-full h-full"
                objectPosition="center top"
                objectFit
              />
            )}
          </div>
          {!noNameTag && (
            <>
              <h3 className="mt-4 text-base font-semibold text-gray-900">
                <Link href={`/product/${product.slug}`}>
                  <a>
                    <span className="absolute inset-0"></span>
                    {product.name}
                  </a>
                </Link>
              </h3>
              <p className="mt-1 text-sm text-gray-500">{`${price} ${product.price?.currencyCode}`}</p>
            </>
          )}
        </>
      )}

      {variant === 'default' && (
        <>
          {process.env.COMMERCE_WISHLIST_ENABLED && (
            <WishlistButton
              className={s.wishlistButton}
              productId={product.id}
              variant={product.variants[0] as any}
            />
          )}

          {product?.images && (
            <div className="w-full overflow-hidden rounded-lg h-96 group-hover:opacity-75 sm:h-auto sm:aspect-w-2 sm:aspect-h-3">
              <Image
                layout="fill"
                alt={product.name || 'Product Image'}
                src={product.images[0]?.url || placeholderImg}
                quality="85"
                className="object-cover object-center w-full h-full"
                objectPosition="center top"
                objectFit
              />
            </div>
          )}
          <ProductTag
            name={product.name}
            price={`${price} ${product.price?.currencyCode}`}
          />
        </>
      )}
    </div>
  )
}

export default ProductCard
